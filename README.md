# Sass-Template

Place sass content into the sass folder

Place an html template that will be using the minified CSS into src folder

Edit both "scss_inputPath" and "scss_outputPath" in the package.json:

Point the scss_inputPath to the sass file that need to be compiled.
Point the scss_outputPath where you want the output to be exited.
The minified file will be output in the same folder than the  scss_outputPath

Use the following command: 

On windows:

	npm run build-css-win
	npm run minify-css-win
On Linux:

```
npm run build-css
npm run minify-css
```

